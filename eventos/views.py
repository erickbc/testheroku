from django.shortcuts import render

# Create your views here.
from eventos.models import Event
from eventos.serializers import EventSerializer
from rest_framework import generics


class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all().order_by('-dateRegistry')
    serializer_class = EventSerializer

class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class ListaCategorias(generics.ListCreateAPIView):
	model=Event
	serializer_class = EventSerializer

	def get_queryset(self):
		idCategoria = self.kwargs["categoria"]
		queryset = Event.objects.filter(category=idCategoria)
		return queryset