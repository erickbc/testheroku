from django.db import models

# Create your models here.
class Event(models.Model):
    KIND_EVENT = (
        ('1', 'Conferencia'),
        ('2', 'Seminario'),
        ('3', 'Congreso'),
        ('4', 'Curso'),
    )
    name = models.CharField(max_length=60)
    category = models.CharField(max_length=1, choices=KIND_EVENT)
    place = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    virtual = models.BooleanField(default=False)
    dateRegistry = models.DateTimeField(auto_now_add=True)



